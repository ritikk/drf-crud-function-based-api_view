import json

import requests

URL = "http://127.0.0.1:8000/studentapi/"


def get_data(id=None):
    data = {}
    if id is not None:
        data['id'] = id
    json_data = json.dumps(data)
    headers = {'content-type': 'application/json'}
    r = requests.get(url=URL, headers=headers, data=json_data)
    data = r.json()  # Extracting the data that came back in r
    print(data)


# get_data(1)


def create_data():
    data = {
        'name': 'Yash Hirapara',
        'roll': 4,
        'city': 'Ahmedabad'
    }

    headers = {'content-Type': 'application/json'}

    json_dta = json.dumps(data)
    r = requests.post(url=URL, headers=headers, data=json_dta)
    data = r.json()
    print(data)


# create_data()


def update_data():
    data = {
        'id': 4,  # To update we must write the id that we want to update
        'name': 'Nimesh Dulam',
        'city': 'Somewhere in Andhra Pradesh'
    }

    json_data = json.dumps(data)
    headers = {'content-type': 'application/json'}
    r = requests.put(url=URL, headers=headers, data=json_data)
    data = r.json()
    print(data)


# update_data()


def delete_data():
    data = {
        'id': 4
    }

    json_data = json.dumps(data)
    headers = {'content-type': 'application/json'}
    r = requests.delete(url=URL, headers=headers, data=json_data)
    data = r.json()
    print(data)

delete_data()
