from django.shortcuts import render
from rest_framework.decorators import api_view
from .models import Student
from .serialzers import StudentSerializer
from rest_framework.response import Response


# Create your views here.
@api_view(['GET', 'POST', 'PUT', 'Delete'])
def student_api(request):
    if request.method == "GET":
        id = request.data.get('id')

        if id is not None:
            stu = Student.objects.get(id=id)
            serialzer = StudentSerializer(stu)
            return Response(serialzer.data)

        stu = Student.objects.all()
        serializer = StudentSerializer(stu, many=True)
        return Response(serializer.data)

    if request.method == "POST":
        serialzer = StudentSerializer(data=request.data)
        if serialzer.is_valid():
            serialzer.save()
            return Response({'msg': 'Data Creation Successful'})
        return Response(serialzer.errors)

    if request.method == "PUT":
        id = request.data.get('id')
        stu = Student.objects.get(id=id)
        serializer = StudentSerializer(stu, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Data updation successfully'})
        return Response(serializer.errors)

    if request.method == "DELETE":
        id = request.data.get('id')
        stu = Student.objects.get(id=id )
        stu.delete()
        return Response({"msg": "Deletion successful"})
